﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO;
using System.Diagnostics;

namespace ChatBot.Controllers
{
    public class ChatBotController : ApiController
    {
        [Route("GetAnswer")]
        public HttpResponseMessage GetAnswer(string userQuestion, string callback)
        {
            string URL = System.Web.Configuration.WebConfigurationManager.AppSettings["azureAPI_URL"].ToString();
            HttpResponseMessage response = new HttpResponseMessage();


            string result = "";
            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(URL);
            httpWebRequest.ContentType = "application/json; charset=utf-8";
            httpWebRequest.Headers.Add("Authorization", System.Web.Configuration.WebConfigurationManager.AppSettings["azureAPI_EndPoint"].ToString());
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                string json = "{\"question\":\"" + userQuestion + "\"}";
                Debug.Write(json);
                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }
            try
            {
                using (var response1 = httpWebRequest.GetResponse() as HttpWebResponse)
                {
                    if (httpWebRequest.HaveResponse && response != null)
                    {
                        using (var reader = new StreamReader(response1.GetResponseStream()))
                        {
                            result = reader.ReadToEnd();
                        }
                    }
                }
            }
            catch (WebException e)
            {
                if (e.Response != null)
                {
                    using (var errorResponse = (HttpWebResponse)e.Response)
                    {
                        using (var reader = new StreamReader(errorResponse.GetResponseStream()))
                        {
                            string error = reader.ReadToEnd();
                            result = error;
                        }
                    }

                }
            }


            response.Content = new StringContent(callback + "(" + result + ")");
            //response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            //Added on 20 Feb, 2020
            response.Content.Headers.Add("set-Cookie", "Secure;SameSite=None");
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/javascript");


            return response;
        }


        [Route("GetPublication")]
        public HttpResponseMessage GetPublication(string pubname, string callback)
        {
            string URL = System.Web.Configuration.WebConfigurationManager.AppSettings["azureAPI_URL_Pub"].ToString();
            HttpResponseMessage response = new HttpResponseMessage();


            string result = "";
            HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(URL);
            httpWebRequest.ContentType = "application/json; charset=utf-8";
            httpWebRequest.Headers.Add("Authorization", System.Web.Configuration.WebConfigurationManager.AppSettings["azureAPI_EndPoint_Pub"].ToString());
            httpWebRequest.Method = "POST";

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                string json = "{\"question\":\"" + pubname + "\"}";
                Debug.Write(json);
                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }
            try
            {
                using (var response1 = httpWebRequest.GetResponse() as HttpWebResponse)
                {
                    if (httpWebRequest.HaveResponse && response != null)
                    {
                        using (var reader = new StreamReader(response1.GetResponseStream()))
                        {
                            result = reader.ReadToEnd();
                        }
                    }
                }
            }
            catch (WebException e)
            {
                if (e.Response != null)
                {
                    using (var errorResponse = (HttpWebResponse)e.Response)
                    {
                        using (var reader = new StreamReader(errorResponse.GetResponseStream()))
                        {
                            string error = reader.ReadToEnd();
                            result = error;
                        }
                    }

                }
            }


            response.Content = new StringContent(callback + "(" + result + ")");
            //response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            //Added on 20 Feb, 2020
            response.Content.Headers.Add("set-Cookie", "Secure;SameSite=None");
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/javascript");

            return response;
        }

        public string GetAuthorizationToken()
        {
            string URL = System.Web.Configuration.WebConfigurationManager.AppSettings["espUrl_WebAPI"].ToString();
            string strAuth_UserID = System.Web.Configuration.WebConfigurationManager.AppSettings["ESP_AuthorizationToken_UserID"].ToString();
            string strAuth_Password = System.Web.Configuration.WebConfigurationManager.AppSettings["ESP_AuthorizationToken_Password"].ToString();

            strAuth_UserID = HttpContext.Current.Server.UrlEncode(strAuth_UserID);
            strAuth_Password = HttpContext.Current.Server.UrlEncode(strAuth_Password);

            try
            {
                URL = URL + "api/Login?UserId=" + strAuth_UserID + "&Password=" + strAuth_Password;
                HttpClient httpClient = new HttpClient();
                httpClient.BaseAddress = new Uri(URL);
                httpClient.DefaultRequestHeaders.Accept.Clear();
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                HttpResponseMessage response = httpClient.GetAsync(URL).Result;

                if (response.IsSuccessStatusCode)
                {
                    var data = response.Content.ReadAsStringAsync().Result;
                    JObject json = (JObject)JsonConvert.DeserializeObject(data);
                    Dictionary<string, object> d = new Dictionary<string, object>(json.ToObject<IDictionary<string, object>>(), StringComparer.CurrentCultureIgnoreCase);
                    return d["accessToken"].ToString();
                }

            }
            catch (Exception ex) { }

            //something failed
            return "";
        }

        [Route("GetUrlDetails")]
        public HttpResponseMessage GetPublicationURLDetails(string pubname, string callback)
        {
            string URL = System.Web.Configuration.WebConfigurationManager.AppSettings["espUrl_WebAPI"].ToString();

            string result = "";

            string apiUrl = URL + "Publication/GetPubDetails/" + pubname;
            HttpClient httpClient = new HttpClient();
            httpClient.BaseAddress = new Uri(apiUrl);
            httpClient.DefaultRequestHeaders.Accept.Clear();

            httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            httpClient.DefaultRequestHeaders.Add("Authorization-Token", GetAuthorizationToken());
            HttpResponseMessage response = httpClient.GetAsync(apiUrl).Result;

            if (response.IsSuccessStatusCode)
            {
                response.Content = new StringContent(callback + "(" + response.Content.ReadAsStringAsync().Result + ")");
                //response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

                //Added on 20 Feb, 2020
                response.Content.Headers.Add("set-Cookie", "Secure;SameSite=None");
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/javascript");
            }

            return response;
        }
    }
}
